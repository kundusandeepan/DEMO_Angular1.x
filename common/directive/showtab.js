/**
 * Created by Sandeepan on 6/12/2016.
 */

angular.module('crmAideApp')
    .directive('showtab',
    function () {
        return {
            link: function (scope, element, attrs) {
                jQuery(element).click(function(e) {
                    e.preventDefault();
                    $(element).tab('show');
                });
            }
        };
    });