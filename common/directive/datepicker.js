/**
 * Created by Sandeepan on 6/4/2016.
 */
angular.module('crmAideApp')
    .directive('datepicker', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            jQuery(element).datepicker({
                dateFormat:'dd/mm/yy',
                onSelect:function (date) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(date);
                    });
                }
            });
        }
    };
});