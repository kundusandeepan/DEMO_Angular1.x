/**
 * Created by Sandeepan on 5/8/2016.
 */
angular.module('crmAideApp')
    .directive('serviceFieldGenerator', function($compile) {

        var ServiceFieldGeneratorController = function ServiceFieldGeneratorController($scope, $log){
            // console.log("ServiceFieldGeneratorController ");
            // $log.log(" say something :");
            // $log.log( $scope);
        };

        return {
            "restrict":"E",
            scope:{
                fieldmetadata:"=",
                datamodel:"=",
                datakey:'='
            },
            //scope:{},//isolate scope
            /*scope:{
                "ng-model" : "="
            },*/
            //template :'Name : {{datasource.name}} {{metadata.length}}'
            controller: ServiceFieldGeneratorController,
            link:function(scope, element,attrs){
                var generatedTemplate ='';
                scope.caption = scope.fieldmetadata['FieldCaption'];
                switch (scope.fieldmetadata.FieldType){
                    case 1:
                        //generatedTemplate = ' text-box';
                        generatedTemplate ='<crm-text-box ng-model="datamodel[datakey]" caption="{{caption}}" placeholder="{{fieldmetadata[FieldCaption]}}"></crm-text-box>';
                        break;
                    case 2:
                        generatedTemplate ='<crm-date-control ng-model="datamodel[datakey]" caption="{{caption}}" placeholder=""></crm-date-control>';
                        //generatedTemplate = ' date control';
                        break;
                    case 3:
                         generatedTemplate ='<crm-number-box ng-model="datamodel[datakey]" caption="{{caption}}" placeholder=""></crm-number-box>';
                        //generatedTemplate = ' number-box';
                        break;
                    case 4:
                        //generatedTemplate = ' text area ';
                        generatedTemplate ='<crm-text-area ng-model="datamodel[datakey]" caption="{{caption}}" placeholder=""></crm-text-area>';
                        break;
                    case 100:
                        //generatedTemplate = 'drop-down-list';
						var optns = scope.fieldmetadata['ListOptions'];
                        var optns2 = scope.fieldmetadata['ListOptions2'];
                        if(angular.isDefined(optns) && optns!=null){
                            scope.listOptions = optns.split("|");
                            console.log(">>>listOptions>>>");
                            console.log(scope.listOptions);
                            generatedTemplate ='<crm-drop-down-list ng-model="datamodel[datakey]" options="listOptions" optexp="x for x in listOptions" defaultLabel="Select One" ></crm-drop-down-list>';
                        }
                        if(angular.isDefined(optns2) && optns2!=null){
                            scope.listOptions = optns2;
                            generatedTemplate ='<crm-drop-down-list ng-model="datamodel[datakey]" options="listOptions" optexp="x.ID as x.DisplayName for x in listOptions track by x.ID" defaultLabel="Select One" ></crm-drop-down-list>';
                        }
                        
                        break;
                    case 200:
                        //contact search
                        generatedTemplate ='<crm-contact-picker-control caption="{{caption}}" ng-model="datamodel[datakey]" caption="{{fieldmetadata[FieldCaption]}}" placeholder="{{fieldmetadata[FieldCaption]}}"></crm-contact-picker-control>';
                        break;
                    default:
                        //generatedTemplate = "default-template-control";
                        break;
                }
                //generatedTemplate = "<div>" + generatedTemplate + "</div>";
                var linkFn = $compile(generatedTemplate);
                var content = linkFn(scope);
                element.append(content);
            }
        };
    });