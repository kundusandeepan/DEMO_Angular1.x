/**
 * Created by Sandeepan on 5/8/2016.
 */
angular.module('crmAideApp')
    .directive('crmContactPickerControl', function($compile, contactService,  $parse) {

        return {
            restrict: 'E',
            scope: false,
            template : "<label>{{caption}}:</label> <contactname /> <img src='assets/img/Calendar.png' class='Calenderimg' title='Calendar'/>",
            link: function (scope, element, attrs, modelController) {
                scope.$watch(attrs.ngModel, function () {
                    var model = scope.$eval(attrs.ngModel);
                    if (angular.isDefined(model)){
                        contactService.getByContactId(model).then(function (data) {
                            element.find('contactname').text(data.FirstName + " " + data.LastName  );
                        });
                        element.find('contactname').text("Contact Id -" + model);
                    }
                });
            }
        };
    });