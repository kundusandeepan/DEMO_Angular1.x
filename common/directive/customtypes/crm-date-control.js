/**
 * Created by Sandeepan on 5/8/2016.
 */
angular.module('crmAideApp')
    .directive('crmDateControl', function($compile) {
        return {
            scope:{
                "ngModel":"=",
                "caption" :"@",
                "placeholder" :"@"
            },
            controller: function($scope) {
                if($scope.ngModel!=null){
                    $scope.localValue = moment($scope.ngModel).toDate();
                }
                else{
                    $scope.localValue = null;
                }
                $scope.updateDate = function () {
                    $scope.ngModel =moment($scope.localValue).toISOString();
                };
            },
            template : "<label>{{caption}}:</label> <input type='date' autocomplete='off' ng-model='localValue' ng-blur='updateDate()' />"
        };
    });