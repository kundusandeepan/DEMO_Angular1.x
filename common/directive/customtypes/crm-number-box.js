/**
 * Created by Sandeepan on 5/8/2016.
 */
angular.module('crmAideApp')
    .directive('crmNumberBox', function($compile) {
        return {
            scope:{
                "ngModel":"=",
                "caption" :"@",
                "placeholder" :"@"
            },
            template : "<label>{{caption}}:</label> <input type='text' only-digits autocomplete='off'  placeholder='{{placeholder}}' ng-model='ngModel' />"
        };
    });