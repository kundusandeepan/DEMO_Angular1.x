/**
 * Created by Sandeepan on 5/8/2016.
 */
angular.module('crmAideApp')
.directive('crmDropDownList', function () {
    return {
        replace: true,
        restrict: 'E',
        scope: false,
        template: function (element, attrs) {
            if (!angular.isDefined(attrs.defaultLabel))
                attrs.defaultLabel = "";
            
            return '<div class="selectBox selector">'+
                    '<label>{{caption}}:</label> ' +
                '<span>{{ ngModel.name || "' + attrs.defaultLabel + '"}}</span>'+
                '<select name="' + attrs.name + '" ng-model="' + attrs.ngModel + '" ng-options="' + attrs.optexp + '"' + ((attrs.required) ? ' required' : '') + '></select>'+
                '</div>';
        },
        link: function (scope, el, attrs) {
            scope.$watch(attrs.ngModel, function () {
                var model = scope.$eval(attrs.ngModel);
                if (angular.isDefined(model) && angular.isDefined(model.name)) {
                    el[0].firstChild.innerText = model.name;
                }
            });
        }
    };
});