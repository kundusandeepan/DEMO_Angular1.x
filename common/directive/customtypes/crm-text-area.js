/**
 * Created by Sandeepan on 5/8/2016.
 */
angular.module('crmAideApp')
    .directive('crmTextArea', function($compile) {
        return {
            scope:{
                "ngModel":"=",
                "caption" :"@"
            },
            template : "<label>{{caption}}:</label> <textarea ng-model='ngModel' autocomplete='off' ></textarea>"
        };
    });