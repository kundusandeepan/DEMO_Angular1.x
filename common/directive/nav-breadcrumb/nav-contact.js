/**
 * Created by Sandeepan on 5/10/2016.
 */
angular.module('crmAideApp')
    .directive('navContact', function($compile,contactService) {
        return {
            "restrict":"E",
            template : "<a ui-sref='contactdetail({householdID:householdid, contactID:contactid})'><contactname /></a>",
            link: function (scope, element, attrs, modelController) {
                element.find('contactname').text("contactname");

                scope.$watch("contactid", function () {
                    var model = scope.contactid;
                    if (angular.isDefined(model) && model>0) {
                        element.find('contactname').text("Contact Id -" + model);
                        contactService.getByContactId(model).then(function (data) {
                            element.find('contactname').text( contactService.getFullName(data));
                        });
                    }
                });


            }
        };
    });