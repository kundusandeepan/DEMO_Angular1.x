/**
 * Created by Sandeepan on 5/10/2016.
 */
angular.module('crmAideApp')
    .directive('navHousehold', function($compile, householdService) {
        return {
            "restrict":"E",
            template : "<a ui-sref='householddetail.contacts({householdID:householdid})'><addresstext /></a>",
            link: function (scope, element, attrs, modelController) {
                element.find('addresstext').text("Address");
                var model = scope.householdid;
                if (angular.isDefined(model) && model>0) {
                    element.find('addresstext').text("Address Id -" + model);
                    householdService.getById(model).then(function (data) {
                        element.find('addresstext').text( householdService.getAddressString(data));
                    });
                }
            }
        };
    });