/**
 * Created by Sandeepan on 5/10/2016.
 */
angular.module('crmAideApp')
    .directive('navSeparator', function($compile) {
        return {
            "restrict":"E",
            template : "&nbsp;&nbsp;>&nbsp;&nbsp;"
        };
    });