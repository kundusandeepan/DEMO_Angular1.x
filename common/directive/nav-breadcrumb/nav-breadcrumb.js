/**
 * Created by Sandeepan on 5/10/2016.
 */
/**
 * Created by Sandeepan on 5/8/2016.
 */
angular.module('crmAideApp')
    .directive('navBreadcrumb', function($compile) {

        var NavigationBreadCrumbController = function NavigationBreadCrumbController($scope, $log){
            //console.log("ServiceFieldGeneratorController ");
        };

        var _fnLink = function(scope, element,attrs){
            var generatedTemplate = '';
            if(angular.isDefined(scope.householdid) && scope.householdid>0 ){
                // make link to household
                if(generatedTemplate.length>0)
                    generatedTemplate += "<nav-separator></nav-separator>";
                generatedTemplate += "<nav-household ></nav-household>";
            }
            if(angular.isDefined(scope.householdid) && scope.householdid>0 && 
                angular.isDefined(scope.contactid) && scope.contactid>0 ){
                // make link to contact
                if(generatedTemplate.length>0)
                    generatedTemplate += "<nav-separator></nav-separator>";
                generatedTemplate +="<nav-contact ></nav-contact>";
            }
            if(generatedTemplate.length>0){
                var linkFn = $compile(generatedTemplate);
                var content = linkFn(scope);
                element.empty().append(content);
            }
            else{
                element.empty();
            }

        };

        return {
            "restrict":"E",
            scope:{
                householdid:"=",
                contactid:"="
            },
            controller: NavigationBreadCrumbController,
            link:function(scope, element,attrs){
                scope.$watch("householdid", function () {
                    _fnLink(scope, element,attrs);
                });
                scope.$watch("contactid", function () {
                    _fnLink(scope, element,attrs);
                });
                _fnLink(scope, element,attrs);
            }
        };
    });