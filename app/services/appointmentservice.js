/**
 * Created by Sandeepan on 7/9/2016.
 */
angular.module( 'crmAideApp')
    .service('appointmentservice', function($http, $q, BASE_URL) {

        var _getAppointments = function () {
            var requestPromise = $http({
                method: "get",
                url: BASE_URL + "api/Appointment"
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };

        var _addAppointment = function(appointment){
            var requestPromise = $http({
                method: "post",
                url: BASE_URL + "api/Appointment",
                data: appointment
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var _updateAppointment = function(appointment){
            var requestPromise = $http({
                method: "put",
                url: BASE_URL + "api/Appointment",
                data: appointment
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var _deleteAppointment = function(appointmentid){
            var requestPromise = $http({
                method: "delete",
                url: BASE_URL + "api/Appointment",
                params:{
                    id: appointmentid
                }
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };


        var handleSuccess = function handleSuccess( response ) {
            return( response.data );
        };
        var handleErrorWithDefaultText = function handleErrorDefaultText (response) {
            $q.resolve("Not defined");
            return( "Not defined" );
        };
        var handleError = function handleError( response ) {
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if (
                ! angular.isObject( response.data ) ||
                ! response.data.message
            ) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        };
        return {
            getAppointments : _getAppointments,
            AddAppointment: _addAppointment,
            UpdateAppointment: _updateAppointment,
            DeleteAppointment: _deleteAppointment
        };
    });