angular.module( 'crmAideApp')
    .factory('Masterdata', function($http, $q , $log, BASE_URL){


        var statesURL = BASE_URL + "api/MasterData/States";
        var countriesURL = BASE_URL + "api/MasterData/Countries";
        var salutationURL = BASE_URL + "api/MasterData/Salutation";
        var serviceOptionsURL = BASE_URL + 'api/MasterData/GetServiceOptions';
        var contactTypesURL = BASE_URL + 'api/MasterData/ContactTypes';


        var handleSuccess = function handleSuccess( response ) {
            return( response.data );
        };
        var handleError = function handleError( response ) {
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if (
                ! angular.isObject( response.data ) ||
                ! response.data.message
            ) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        };
        var _getServiceOption= function getServiceOption() {
            var requestPromise = $http(
                {
                    method: 'GET',
                    cache: true,
                    url: serviceOptionsURL
                });
            
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var _getContactTypes = function getContactTypes() {
            var requestPromise = $http(
                {
                    method: 'GET',
                    cache: true,
                    url: contactTypesURL
                });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var _getSalutation = function getSalutation() {
            var requestPromise = $http(
                {
                    method: 'GET',
                    cache: true,
                    url: salutationURL
                });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var _getStates = function getStates() {
            var requestPromise = $http(
                {
                    method: 'GET',
                    cache: true,
                    url: statesURL
                });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var _getCountries = function getCountries() {
            var requestPromise = $http(
                {
                    method: 'GET',
                    cache: true,
                    url: countriesURL
                });
            return( requestPromise.then( handleSuccess, handleError ) );
        };

        var _filterById = function(filterSource, filterParam, keyToCompare, keyNameToRetrun){
            for(var i=0;i<filterSource.length;i++){
                if(filterSource[i][keyToCompare] === filterParam)
                {
                    return filterSource[i][keyNameToRetrun];
                }
            }
            return "";
        };
        var _getContactTypeNameById = function(contactTypeId){
            var d = $q.defer();
            _getContactTypes().then(function(data){
                var returnValue = _filterById(data, contactTypeId, "ContactTypeID", "ContactName");
                d.resolve(returnValue);
            },function failure(reason){
                d.reject(reason);
            });
            return d.promise;
        };
        var _getServiceOptionNameById = function(serviceOptionId){
            var d = $q.defer();
            _getServiceOption().then(function(data){
                var returnValue = _filterById(data, serviceOptionId, "ServiceID", "ServiceName");
                d.resolve(returnValue);
            },function failure(reason){
                d.reject(reason);
            });
            return d.promise;
        };
        var _getCountryNameById = function(countryId){
            var d = $q.defer();
            _getCountries().then(function(data){
                var returnValue = _filterById(data, countryId, "CountryID", "CountryName");
                d.resolve(returnValue);
            },function failure(reason){
                d.reject(reason);
            });
            return d.promise;
        };
        var _getSalutationNameById = function(salutationId){
            var d = $q.defer();
            _getSalutation().then(function(data){
                var returnValue = _filterById(data, salutationId, "SalutationTypeID", "SalutationTypeName");
                d.resolve(returnValue);
            },function failure(reason){
                d.reject(reason);
            });
            return d.promise;
        };
        var _getStateNameById = function(stateId, countryId){
            var d = $q.defer();
            _getStates().then(function(states){

                for(var i=0;i<states.length;i++){
                    if(states[i].StateID == stateId && states[i].CountryID == countryId)
                    {
                        d.resolve(states[i].StateName);
                    }
                }
                d.resolve("");
            },function failure(reason){
                d.reject(reason);
            });
            return d.promise;
        };
        var _getStatesByCountryId= function(countryId){
            var d = $q.defer();
            _getStates().then(function(states){
                var arr = [];
                for(var i=0;i<states.length;i++){
                    if(states[i].CountryID == countryId)
                    {
                        arr.push( states[i]);
                    }
                }
                d.resolve(arr);
            },function failure(reason){
                d.reject(reason);
            });
            return d.promise;
        };

        var _getServiceOptionFieldsById = function(serviceOptionId){
            var d = $q.defer();
            _getServiceOption().then(function(data){
                var returnValue ="";// _filterById(data, serviceOptionId, "ServiceID", "ServiceFields");

                for(var i=0;i<data.length;i++){
                    if(data[i]["ServiceID"] == serviceOptionId)
                    {
                        returnValue= data[i]["ServiceFields"];
                    }
                }
                d.resolve(returnValue);
            },function failure(reason){
                d.reject(reason);
            });
            return d.promise;
        };

        var _loadAllMasterData = function () {
            var promiseServiceOptions = _getServiceOption();
            var promiseGetSalutation = _getSalutation();
            var promiseCountry = _getCountries();
            var promiseState = _getStates();
            $q.all([promiseServiceOptions,promiseGetSalutation,promiseCountry,promiseState])
                .then(function () {

                });
        };
        _loadAllMasterData();

        return{
            getStates : _getStates,
            getStateNameById: _getStateNameById,
            getStatesByCountryId: _getStatesByCountryId,
            getCountries : _getCountries,
            getCountryNameById: _getCountryNameById,
            getSalutation: _getSalutation,
            getSalutationNameById: _getSalutationNameById,
            getServiceOption : _getServiceOption,
            getServiceOptionNameById: _getServiceOptionNameById,
            getServiceOptionFieldsById: _getServiceOptionFieldsById,
            getContactTypes : _getContactTypes,
            getContactTypeNameById : _getContactTypeNameById
        };
    });