/**
 * Created by Sandeepan on 8/13/2016.
 */
angular.module('crmAideApp')
    .factory('httpRequestTracker', ['$http', function ($http) {

        var httpRequestTracker = {};
        httpRequestTracker.hasPendingRequests = function () {
            return $http.pendingRequests.length > 0;
        };

        return httpRequestTracker;
    }]
);