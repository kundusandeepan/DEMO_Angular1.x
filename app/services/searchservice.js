angular.module( 'crmAideApp')
    .service('searchService', function($http, $q, BASE_URL) {

        var _searchContacts = function(query){
            var requestPromise = $http({
                method: "get",
                url: BASE_URL + "api/Search",
                 params: {
                     query: query
                 }
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var handleSuccess = function handleSuccess( response ) {
            return( response.data );
        };
        var handleError = function handleError( response ) {
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if ( ! angular.isObject( response.data ) || ! response.data.message ) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        };
        return {
            searchContacts : _searchContacts
        };
    });