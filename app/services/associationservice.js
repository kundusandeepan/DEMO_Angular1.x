angular.module( 'crmAideApp')
    .service('associationservice', function($http, $q, BASE_URL) {

        // var _getAssociation = function (contactId) {
        //     var requestPromise = $http({
        //         method: "get",
        //         url: BASE_URL + "api/Association",
        //         params: {
        //             contactId: contactId
        //         }
        //     });
        //     return( requestPromise.then( handleSuccess, handleError ) );
        // };

        var _addAssociation = function(association){
            var requestPromise = $http({
                method: "post",
                url: BASE_URL + "api/Association",
                data: association
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var _deleteAssociation = function(association){
            var requestPromise = $http({
                method: "delete",
                url: BASE_URL + "api/Association",
                params:{
                    PrimaryContactID: association.PrimaryContactID,
                    RelationContactID: association.RelationContactID
                }
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };


        var handleSuccess = function handleSuccess( response ) {
            return( response.data );
        };
        var handleErrorWithDefaultText = function handleErrorDefaultText (response) {
            $q.resolve("Not defined");
            return( "Not defined" );
        };
        var handleError = function handleError( response ) {
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if (
                ! angular.isObject( response.data ) ||
                ! response.data.message
            ) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        };
        return {
            // getAssociation : _getAssociation
            AddAssociation: _addAssociation,
            DeleteAssociation: _deleteAssociation
        };
    });