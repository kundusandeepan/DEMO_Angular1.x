/**
 * Created by Sandeepan on 5/6/2016.
 */
angular.module( 'crmAideApp')
    .service('breadcrumbservice', function($http, $q, BASE_URL, Masterdata, householddetail) {

        var promiseServiceOptions = Masterdata.getServiceOption();
        promiseServiceOptions.then(function (data) {
            //$scope.serviceOption = data;
        });

        var promiseGetSalutation = Masterdata.getSalutation();
        promiseGetSalutation.then(function(data){
            //$scope.Salutations = data;
        });

        var promiseCountry = Masterdata.getCountries();
        var promiseState = Masterdata.getStates();

        $scope.genderList = [
            {"genderId" : true, "genderName" : "Male"},
            {"genderId" : false, "genderName" : "Female"}
        ];
        var promiseGetByContactId = contactService.getByContactId($stateParams.contactID);

        promiseGetByContactId.then(function (data) {
            var obj ={};
            obj.contact = data;
            if(obj.contact.DateOfBirth !== undefined && $scope.contact.DateOfBirth!=null)
            {
                obj.DOB = parseJsonDate($scope.contact.DateOfBirth);
            }
            //$log.log("get contact by id");
            //$log.log("$scope.contact " +  $scope.contact.IsMale);
            var promiseHouseholdAddress = householdService.getById(obj.contact.AddressID)
                .then(function (data) {
                    obj.household = data;
                    obj.addressdetail = _addressFormatter(obj.household);
                });
        });

        $q.all([promiseGetByContactId,promiseServiceOptions,promiseGetSalutation,promiseCountry,promiseState])
            .then(function () {

            });

        var _addressFormatter = function(address){
            var strn;
            if(address!=null){
                strn = address.Street1;
                if(address.Street2 !=null) {
                    strn += ", " + address.Street2;
                }
                if(address.City !=null){
                    strn += ", " + address.City;
                }
                if(address.Zip !=null){
                    strn += ", " + address.Zip;
                }
                if(address.StateID !=null){
                    strn += ", " + Masterdata.getStateNameById(address.StateID, address.CountryID);
                }
                if(address.CountryID !=null){
                    strn += ", " + Masterdata.getCountryNameById(address.CountryID);
                }
            }
            return strn;
        };



        var _getAddressPart = function (addressid) {
            return {
                "ui-sref":householddetail.contacts({ householdID: addressid }),
                "caption" :' address caption - text to be formatted'
            };
        };
        var _getContactNamePart =function (customerid) {
            return {
                "ui-sref": householddetail.contacts({ householdID: customerid }),
                "caption" : 'customer name part'
            };
        };

        return {
            GetAddressPart: _getAddressPart,
            GetContactNamePart : _getContactNamePart
        };
    });