
            return( requestPromise.then( handleSuccessStringToJson, handleError ) );
        };
        var _getGraph = function (data) {
            var requestPromise = $http({
                method: "post",
                url: BASE_URL + "api/report/graph",
                data: data
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var _getMetadata= function (reportid) {
            var requestPromise = $http({
                method: "get",
                url: BASE_URL + "api/report/parameters",
                params: {
                    reportid: reportid
                }
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var handleSuccess = function handleSuccess( response ) {
            return( response.data );
        };
        var handleSuccessStringToJson= function handleSuccess( response ) {

            var jsondata = [];
            if(response.data.length >0){
                jsondata = angular.fromJson(response.data);
            }
            // console.log(jsondata);
            return jsondata;
        };
        var handleError = function handleError( response ) {
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if ( ! angular.isObject( response.data ) || ! response.data.message ) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        };
        return {
            GetTree : _getTree,
            GetReport : _getReport,
            GetGraph: _getGraph,
            GetMetadata: _getMetadata
        };
    });