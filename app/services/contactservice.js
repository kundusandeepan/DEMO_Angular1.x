angular.module( 'crmAideApp')
    .service('contactService', function($http, $q, BASE_URL) {

        var _getByAddressId = function(recordId){
            var d = $q.defer();
            var requestPromise = $http({
                method: "get",
                url: BASE_URL + "api/Contact/GetContactsByAddressId",
                 params: {
                    addressId: recordId
                 }
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        
        var _getAllContact = function(recordId){
            var d = $q.defer();
            var requestPromise = $http({
                method: "get",
                url: BASE_URL + "api/Contact/GetAllContacts"
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var _getByContactId = function (contactId) {
            var requestPromise = $http({
                method: "get",
                url: BASE_URL + "api/Contact",
                params: {
                    contactId: contactId
                }
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var _addContact = function(address){
            var requestPromise = $http({
                method: "post",
                url: BASE_URL + "api/Contact",
                data: address
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };

        var _updateContact = function(address){
            var requestPromise = $http({
                method: "put",
                url: BASE_URL + "api/Contact",
                data: address
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        
        var _deleteContact = function(contactid){
            var requestPromise = $http({
                method: "delete",
                url: BASE_URL + "api/Contact",
                params:{
                    contactid: contactid
                }
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };

        var _getFullName = function(contact){
            var retVal = "";
            if(contact!=null){
                if(contact.FirstName){
                     retVal =  contact.FirstName;
                }
                if(contact.MiddleName){
                    if(retVal.length>0)
                        retVal += " ";
                    retVal +=  contact.MiddleName;
                }
                if(contact.LastName){
                    if(retVal.length>0)
                        retVal += " ";
                    retVal +=  contact.LastName;
                }
            }
            return retVal;
        };

        var handleSuccess = function handleSuccess( response ) {
            return( response.data );
        };
        var handleError = function handleError( response ) {
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if (
                ! angular.isObject( response.data ) ||
                ! response.data.message
            ) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        };
        return {
            getByAddressId : _getByAddressId,
            getByContactId : _getByContactId,
            updateContact : _updateContact,
            getFullName : _getFullName,
            deleteContact : _deleteContact,
            addContact : _addContact,
            getAllContact: _getAllContact
        };
    });