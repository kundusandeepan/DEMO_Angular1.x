angular.module( 'crmAideApp')
    .service('authService', function($http, $q, $rootScope, BASE_URL, $log ) {
        var _userInfo = null;
        var _login = function(username, password){
            var requestPromise = $http({
                method: "post",
                url: BASE_URL + "api/Authenticate",
                 data: {
                     "Username": username,
                     "Password":password
                 }
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var _logout = function(){
            var requestPromise = $http({
                method: "delete",
                url: BASE_URL + "api/Authenticate"
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var handleSuccess = function handleSuccess( response ) {
            return( response.data );
        };
        var handleError = function handleError( response ) {
            if (
                ! angular.isObject( response.data ) ||
                ! response.data.message
            ) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        };
        var _clearCredentials = function () {
            $rootScope.globals = {};
            //$cookieStore.remove('globals');
            //$http.defaults.headers.common.Authorization = 'Session';
            $http.defaults.headers.common.Authorization = null;
            _userInfo =null;

        };
        var _setCredentials = function (credential) {
            $rootScope.globals = {
                currentUser: credential
            };
            _userInfo = credential;
            $log.log(_userInfo);

            $http.defaults.headers.common['Authorization'] = 'Session ' + credential.AuthToken;

            //$cookieStore.put('globals', $rootScope.globals);
        };
        var _getUserInfo = function () {
            return _userInfo;
        };
        var _isLoggedIn = function () {
            return (_userInfo!=null) ;
        };
        return {
            login: _login,
            logout : _logout,
            setCredentials: _setCredentials,
            clearCredentials : _clearCredentials,
            getUserInfo : _getUserInfo,
            isLoggedIn : _isLoggedIn
        };
    });