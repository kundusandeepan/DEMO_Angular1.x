/**
 * Created by Sandeepan on 5/6/2016.
 */
angular.module( 'crmAideApp')
    .service('contactservicedataservice', function($http, $q , BASE_URL) {
        
        var _getContactServiceFieldsData= function (contactserviceid) {
            var requestPromise = $http({
                method: "get",
                url: BASE_URL + "api/ContactServiceData",
                params: {
                    "contactserviceid" : contactserviceid
                }
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };

        var _addServiceData = function(contactid, serviceid, description){
            var requestPromise = $http({
                method: "post",
                url: BASE_URL + "api/ContactServiceData/",
                params: {
                    "contactid" : contactid,
                    "serviceid" : serviceid,
                    "description" : description
                }
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };


        var _updateSubscription = function(subscriptionid, data){
            var requestPromise = $http({
                method: "put",
                url: BASE_URL + "api/ContactServiceData/",
                data: {
                    SubscriptionId:subscriptionid,
                    FieldValuePair : data
                }
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };

        var _deleteSubscription = function(contactserviceid){
            var requestPromise = $http({
                method: "delete",
                url: BASE_URL + "api/ContactServiceData",
                params:{
                    contactserviceid: contactserviceid
                }
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };

        var handleSuccess = function handleSuccess( response ) {
            return( response.data );
        };
        var handleError = function handleError( response ) {
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if (
                ! angular.isObject( response.data ) ||
                ! response.data.message
            ) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        };
        return {
            getContactServiceFieldsData: _getContactServiceFieldsData,
            addServiceData : _addServiceData,
            deleteSubscription: _deleteSubscription,
            updateSubscription:_updateSubscription
        };
    });