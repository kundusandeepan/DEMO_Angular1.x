angular.module( 'crmAideApp')
    .service('householdService', function($http, $q , BASE_URL, Masterdata) {

        var _addressFormatter = function(address){
            var strn;
            if(address!=null){
                strn = address.Street1;
                if(address.Street2 !=null) {
                    strn += ", " + address.Street2;
                }
                if(address.City !=null){
                    strn += ", " + address.City;
                }
                if(address.Zip !=null){
                    strn += ", " + address.Zip;
                }
                
                if(address.state !=null){
                    strn += ", " + address.state;
                }
                if(address.country !=null){
                    strn += ", " + address.country;
                }
            }
            return strn;
        };
        var _getAll = function(){
            var d = $q.defer();
            $http({
                method: "get",
                url: BASE_URL + "api/Household"
            }).then( function success(response){
                //return( response.data );
                var data  = response.data;
                var recordRowDataPromises = [];

                angular.forEach(data, function (record) {

                    var promiseCountry = Masterdata.getCountryNameById(record.CountryID);
                    recordRowDataPromises.push(promiseCountry);
                    promiseCountry.then(function(data){
                        record.country = data;
                    });

                    var promiseState = Masterdata.getStateNameById(record.StateID, record.CountryID);
                    recordRowDataPromises.push(promiseState);
                    promiseState.then(function(data){
                        record.state = data;
                    });

                    var promiseAddress = $q.defer();
                    recordRowDataPromises.push(promiseAddress);

                    $q.all([promiseCountry,promiseState]).then(function(){
                        record.address =_addressFormatter(record);
                        promiseAddress.resolve(record.address);
                    });
                });

                $q.all(recordRowDataPromises).then(function(){
                    d.resolve(response.data );
                });

            }, function(response){
                handleError(response);
                d.reject(response);
            } );
            return d.promise;
        };

        var _getById = function(recordId){
            var requestPromise = $http({
                method: "get",
                url: BASE_URL + "api/Household/" + recordId
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };

        var _updateAddress = function(address){
            var requestPromise = $http({
                method: "put",
                url: BASE_URL + "api/Household/",
                 data: address
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };

        var _addAddress = function(address){
            var requestPromise = $http({
                method: "post",
                url: BASE_URL + "api/Household/",
                data: address
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };

        var _getCities = function (countryId, stateId, query) {
            var requestPromise = $http({
                method: "post",
                url: BASE_URL + "api/Household/GetCities",
                data: {
                    "CountryId":countryId,
                    "StateId": stateId,
                    "Query": query
                }
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };
        var _deleteAddress= function (addressid) {
            var requestPromise = $http({
                method: "delete",
                url: BASE_URL + "api/Household?addressid=" + addressid
            });
            return( requestPromise.then( handleSuccess, handleError ) );
        };

        var handleSuccess = function handleSuccess( response ) {
            return( response.data );
        };
        var handleError = function handleError( response ) {
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if (
                ! angular.isObject( response.data ) ||
                ! response.data.message
            ) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        };

        return {
            getAll: _getAll,
            getById : _getById,
            updateAddress: _updateAddress,
            addAddress:_addAddress,
            deleteAddress: _deleteAddress,
            getCities : _getCities,
            getAddressString : _addressFormatter
        };
    });