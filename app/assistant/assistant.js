/**
 * Created by Sandeepan on 5/10/2016.
 */
angular.module( 'crmAideApp.assistant', [
        'ui.router',
        'ui.bootstrap'
    ])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'assistant', {
            url: '/assistant',
            views: {
                "main": {
                    controller: 'AssistantController',
                    templateUrl: 'assistant/assistant.tpl.html'
                }
            },
            data:{ pageTitle: 'Assistant' }
        });
        $stateProvider.state( 'assistant.appointment', {
            url: '/appointment',
            views: {
                "assistant-appointment": {
                    controller: 'AppointmentController',
                    templateUrl: 'assistant/appointment.tpl.html'
                }
            },
            data:{ pageTitle: 'Assistant' },
            resolve:{
                appointmentdata:['appointmentservice','$log',
                    function(appointmentservice, $log){
                        return  appointmentservice.getAppointments();
                    }]
            }
        });
        $stateProvider.state( 'assistant.task', {
            url: '/task',
            views: {
                "assistant-task": {
                    controller: 'TaskController',
                    templateUrl: 'assistant/task.tpl.html'
                }
            },
            data:{ pageTitle: 'Assistant' }
        });
    })

    .controller( 'AssistantController', function AssistantController( $log,$scope ) {
        $log.log('AssistantController');
    })
    .controller( 'AppointmentController', function AppointmentController( $scope,
                                                                          $compile,
                                                                          $timeout,
                                                                          appointmentdata,
                                                                          $log,
                                                                          ngDialog,
                                                                          uiCalendarConfig ) {

        $scope.events = [];
        $scope.events.slice(0,$scope.events.length);
        angular.forEach(appointmentdata,function(value){
            var newevent = {};
            newevent.start = moment(value.StartAt).toDate();
            newevent.end = moment(value.EndAt).toDate();
            newevent.allDay = value.IsFullDay;
            newevent.title = value.Title;
            newevent.description = value.Description;
            newevent.id = value.Id;
            newevent.stick = true;
            $scope.events.push(newevent);
        });

        $log.log('AppointmentController');
        $log.log(appointmentdata);
        var isFirstTime=true;
        /* alert on eventClick */
        $scope.alertOnEventClick = function( appointmentdata, jsEvent, view){
            //$log.log(appointmentdata.title + ' was clicked ');

            var dialog2 = ngDialog.open({
                template: 'assistant/addeditappointment.tpl.html',
                className: 'ngdialog-theme-default',//custom-width-1180',
                controller: 'AddEditAppoinment',
                data : appointmentdata
            });

        };

        $scope.addEvent = function(view,calendar) {
            // alert("add event");
            var appointmentdata={};
            var dialog2 = ngDialog.open({
                template: 'assistant/addeditappointment.tpl.html',
                className: 'ngdialog-theme-default',//custom-width-1180',
                controller: 'AddEditAppoinment',
                data : appointmentdata
            });
        };
        /* Change View */
        $scope.changeView = function(view,calendar) {
            uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
        };
        /* Change View */
        $scope.renderCalender = function(calendar) {
            if(uiCalendarConfig.calendars[calendar]){
                uiCalendarConfig.calendars[calendar].fullCalendar('render');
            }
        };
        /* Render Tooltip */
        $scope.eventRender = function( event, element, view ) {
            element.attr({'uib-tooltip': event.description,
                'uib-tooltip-append-to-body': true});
            $compile(element)($scope);
        };
        /* config object */
        $scope.uiConfig = {
            calendar:{
                height: 450,
                editable: false,
                header:{
                    left: 'title',
                    center: '',
                    right: 'today prev,next'
                },
                eventClick: $scope.alertOnEventClick,
                eventRender: $scope.eventRender
            }
        };
        // /* event sources array*/
        $scope.eventSources = [$scope.events];
    })
    .controller( 'TaskController', function TaskController( $scope,$log ) {
        $log.log('TaskController');
    })
    .controller( 'AddEditAppoinment',function AddEditAppoinment(
        $scope,
        $state,
        $stateParams,
        $log,
        ngDialog,
        appointmentservice
    ){
       //$log.log(" inside >AddEditAppoinment");
        $scope.addeditappointment ={
            id:$scope.ngDialogData.id,
            title : angular.isDefined($scope.ngDialogData.title)?$scope.ngDialogData.title : "New Appointment",
            description: angular.isDefined($scope.ngDialogData.description)? $scope.ngDialogData.description: "",
            start: angular.isDefined($scope.ngDialogData.start)? moment($scope.ngDialogData.start).toDate() : moment().toDate(),
            end: angular.isDefined($scope.ngDialogData.end)? moment($scope.ngDialogData.end).toDate() : moment().add(30,"minute").toDate(),
            allDay: $scope.ngDialogData.allDay
        };
        //$log.log($scope.addeditappointment);

        $scope.isOpenStart = false;
        $scope.isOpenEnd = false;

        $scope.openCalendarStart = function(e) {
            e.preventDefault();
            e.stopPropagation();
            $scope.isOpenStart = true;
        };
        $scope.openCalendarEnd = function(e) {
            e.preventDefault();
            e.stopPropagation();
            $scope.isOpenEnd = true;
        };

        $scope.deleteAppointment = function () {
          // alert("delete invoked");
            $log.log($scope.addeditappointment);
            if(angular.isDefined($scope.addeditappointment.id)){
                appointmentservice.DeleteAppointment($scope.addeditappointment.id).then(function () {
                    ngDialog.close();
                    $state.reload();
                },function(){
                    ngDialog.close();
                    $state.reload();
                });
            }else{
                alert("Cannot delete, ID not found");
            }
        };

        $scope.update= function () {
          //save
            $log.log("$scope.addeditappointment");
            $log.log($scope.addeditappointment);
            $scope.addeditappointment.start = moment($scope.addeditappointment.start).utc().format();
            $scope.addeditappointment.end = moment($scope.addeditappointment.end).utc().format();

            if($scope.addeditappointment.start>$scope.addeditappointment.end ){
                alert("Cannot save. Start Time cannot be greater than End Time. Try again");
                ngDialog.close();
                $state.reload();
            }
            // var appointmentdata={};
            // data ={
            //     id:$scope.ngDialogData.id,
            //     title : $scope.ngDialogData.title,
            //     description: $scope.ngDialogData.description,
            //     start: $scope.ngDialogData.start.toDate(),
            //     end: $scope.ngDialogData.end.toDate(),
            //     allDay: $scope.ngDialogData.allDay
            // };

            if(!angular.isDefined($scope.addeditappointment.id) || $scope.addeditappointment.id<=0){
                $log.log("insert");
                appointmentservice.AddAppointment($scope.addeditappointment).then(function(){
                    $log.log("inserted");
                    ngDialog.close();
                    $state.reload();
                },function () {
                    $log.log("failed");
                    ngDialog.close();
                    $state.reload();
                });
            }
            else{
                $log.log("update");
                appointmentservice.UpdateAppointment($scope.addeditappointment).then(function(){
                   $log.log("updated");
                    ngDialog.close();
                    $state.reload();
                },function () {
                    $log.log("failed");
                    ngDialog.close();
                    $state.reload();
                });
            }

        };
    })

;
