/**
 * Created by Sandeepan on 4/9/2016.
 */
angular.module( 'crmAideApp.householddetail', [
        'ui.router',
        'ui.bootstrap'
    ])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'householddetail', {
            url: '/household/:householdID',
            views: {
                "main": {
                    controller: 'HouseholdDetailCtrl',
                    templateUrl: 'household/householddetails.tpl.html'
                }
            },
            data:{ pageTitle: 'Household Details' },
            resolve:{
                allcountriesdata:['Masterdata',  function( Masterdata){
                    return Masterdata.getCountries();
                }],
                allstatesdata:['Masterdata',  function( Masterdata){
                    return Masterdata.getStates();
                }],
                householddata :['$stateParams','householdService',function($stateParams,householdService){
                    if($stateParams.householdID>-1){
                        //try fetching data
                        return householdService.getById($stateParams.householdID);
                    }else{
                        //new record
                        return { CountryID:1,StateID:1};
                    }
                }],
                applicablestates:['Masterdata','householddata',  function( Masterdata,householddata){
                    return Masterdata.getStatesByCountryId(householddata.CountryID);
                    //return Masterdata.getStates();
                }]
            }
        });
        $stateProvider.state( 'householddetail.contacts', {
            url: '/contacts',
            views: {
                "householdcontacts": {
                    controller: 'HouseholdContactsCtrl',
                    templateUrl: 'household/householdcontacts.tpl.html'
                }
            },
            //data:{ pageTitle: 'Household Details' }
            resolve: {
                allsalutationdata:['Masterdata',  function( Masterdata){
                    return Masterdata.getSalutation();
                }],
                contactdata: ['contactService','$stateParams', function (contactService,$stateParams) {
                    return contactService.getByAddressId($stateParams.householdID);
                }]
            }
        });
    })

    .controller( 'HouseholdDetailCtrl', function HouseholdDetailCtrl(
        $scope ,
        $q,
        $location,
        $log,
        $stateParams,
        householdService,
        Masterdata,
        allcountriesdata,
        allstatesdata,
        householddata,
        applicablestates
        ) {
        $scope.householdid = $stateParams.householdID;
        $scope.household = householddata;
        $scope.Countries = allcountriesdata;
        $scope.States = applicablestates;
        $scope.selectedCountryID = $scope.household.CountryID;

        $scope.updateStateList = function(){
            $scope.household.CountryID = $scope.selectedCountryID;
            Masterdata.getStatesByCountryId($scope.selectedCountryID).then(function(data){
                $scope.States = data;
            });
        };

        $scope.Update=function () {
            if($scope.household.AddressID > 0) {
                householdService.updateAddress($scope.household).then(function () {
                    alert("Address updated");
                }, function () {
                    alert("Address update failed");
                });
            }
            else{
                householdService.addAddress($scope.household).then(function (response) {
                    //$location.path('/household/' + response.AddressID +"/contacts");
                    //$state.go("householddetail.contacts",{"householdID" : response.AddressID});
                    $state.reload();
                }, function () {
                    alert("Address insert failed");
                });
            }
        };

        $scope.cities = function(cityName) {
            return householdService.getCities($scope.household.CountryID, $scope.household.StateID, cityName).then(function(response){
                return response;
            });
        };
    })

    .controller('HouseholdContactsCtrl',function HouseholdContactsCtrl( $scope,
                                                                        $location,
                                                                        $log,
                                                                        $state,
                                                                        $stateParams,
                                                                        contactService,
                                                                        Masterdata,
                                                                        $filter,
                                                                        contactdata,
                                                                        allsalutationdata){
        var recordid = $stateParams.householdID;

        $scope.householdid = recordid;

        $scope.showContactDetail = function(householdID, contactid){
            $state.go("contactdetail",{
                "contactID": contactid,
                "householdID" : householdID
            });
        };
        $scope.getAssociation = function (contactId) {
            return "Not Defined";
        };

        $scope.contacts = contactdata;
        $scope.salutationFormatter = function(salutationId){
            var filteredrecord = $filter('filter')(allsalutationdata, { SalutationTypeID: salutationId });
            if(filteredrecord!=null && filteredrecord.length>0) return filteredrecord[0].SalutationTypeName;
            return "";
        };

        $scope.genderText = function genderText(isMale) {
            if (isMale === true) {
                return "Male";
            }
            return "Female";
        };

        $scope.remove = function (contactId) {
            var result = confirm("Do you want to delete?");
            if (result) {
                //Logic to delete the item
                contactService.deleteContact(contactId).then(function(){
                    alert("Record deleted");
                    $state.reload();
                }, function () {
                    alert("Could not delete");
                });
            }
        };

        $scope.AddContact = function () {
            $state.go("addcontact",
                {
                    //"contactID": -1,
                    "householdID" :$stateParams.householdID
                }
            );
        };

    })
;
