/**
 * Created by Sandeepan on 4/9/2016.
 */
angular.module( 'crmAideApp.household', [
        'ui.router',
        'ui.bootstrap'
    ])
    .config(function config( $stateProvider ) {
        $stateProvider.state( 'household', {
            url: '/household',
            views: {
                "main": {
                    controller: 'HouseholdCtrl',
                    templateUrl: 'household/household.tpl.html'
                }
            },
            data:{ pageTitle: 'Household' },
            resolve:{
                householddata:['householdService',
                    function(householdService){
                        return householdService.getAll();
                }]
            }
            /*,
            resolve:{
                auth:["$q","authService" ,function($q, authService){
                    var userInfo = authService.getUserInfo();
                    if(userInfo){
                        return $q.when(userInfo);
                    }
                    else{
                        return $q.reject("notauthenticated");//{authenticated:false});
                    }
                }]
            }*/
        });
    })

    .controller( 'HouseholdCtrl', function HouseholdCtrl(
        $scope ,
        $location, $state, $log,
        householdService,
        $q, 
        Masterdata, 
        householddata) {

        $scope.households = householddata;

        $scope.rowcount =0;
        $scope.pagesize = 10;
        $scope.pagenumber = 1;
        $scope.pageBegin = 0;
        $scope.pageLimit = $scope.pagesize;
        $scope.maxNumberOfPages = 1;

        var setPagingInformation = function(){
            $scope.maxNumberOfPages = Math.ceil($scope.rowcount/$scope.pagesize);
            if( $scope.pagenumber>$scope.maxNumberOfPages) {
                $scope.pagenumber = $scope.maxNumberOfPages;
            }
            if($scope.pagenumber === 0) {
                $scope.pagenumber=1;
            }
            $scope.pageBegin = ($scope.pagenumber -1) * $scope.pagesize;
            $scope.pageLimit = $scope.pagesize;
        };
        setPagingInformation();
        $scope.changePageSize= function(){
            setPagingInformation();
        };
		$scope.changePageNumber= function(){
            setPagingInformation();
        };
        $scope.previousPage = function () {
            if($scope.pagenumber>1) {
                $scope.pagenumber = $scope.pagenumber - 1;
                setPagingInformation();
            }
        };
        $scope.nextPage = function () {
            if ($scope.pagenumber < $scope.maxNumberOfPages) {
                $scope.pagenumber = $scope.pagenumber + 1;
                setPagingInformation();
            }
        };
        $scope.$watchCollection ('filteredhouseholds', function(newval, oldval) {
            $scope.rowcount = newval!=null? newval.length:0;
            setPagingInformation();
        });


        $scope.household = householddata;

        //$scope.addressFormatter = _addressFormatter;
        $scope.showHouseholdDetail = function(householdId){
            //$state.go("householddetail",{"householdID" : householdId});
            $state.go("householddetail.contacts",{"householdID" : householdId});
        };
        $scope.AddAddress= function () {
            $state.go("householddetail.contacts",{"householdID" : -1});
            //$location.path('/household/' + -1 );
        };

        $scope.remove = function (address) {
            var result = confirm("Do you want to delete?");
            if (result) {
                //Logic to delete the item
                householdService.deleteAddress(address.AddressID).then(function(){
                    alert("Record deleted");
                    //$scope.refresh();
                    $state.reload();
                }, function () {
                    alert("Could not delete");
                });
            }
        };
    })
;
