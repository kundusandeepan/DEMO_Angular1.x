angular.module( 'crmAideApp.auth', [
        'ui.router'
    ])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'auth',
            {
                url: '/auth',
                views: {
                    "main": {
                        controller: 'AuthCtrl',
                        templateUrl: 'auth/authlogin.tpl.html'
                    }
                },
                data:{ pageTitle: 'Authenticate' }
            }
        );
        /*$stateProvider.state( 'auth.logout',
            {
                url: '/auth/logout'
            }
        );*/
    })

    .controller( 'AuthCtrl', function AuthCtrl( $scope, $state, $log, authService ) {
        authService.clearCredentials();	
        $scope.errorMessage = "";
		$scope.username= "adminuser01";
		$scope.password = "password";
        $scope.login =  function(){
            authService.login($scope.username,$scope.password )
                .then(function(response){
                    authService.setCredentials(response);
                    $state.go("home");
                }, function fail(errresponse){
                    $scope.errorMessage = "Username or Password is incorrect !";
                    //$log.log(" auth failure");
                });
            //alert("login click : " + $scope.username + " - " + $scope.password);
        };
    })

;
