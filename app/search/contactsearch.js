/**
 * Created by Sandeepan on 4/26/2016.
 */
angular.module( 'crmAideApp.contactsearch', [
        'ui.router',
        'ui.bootstrap'
    ])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'contactsearch', {
            url: '/searchcontact/:searchquery',
            views: {
                "main": {
                    controller: 'ContactSearchController',
                    templateUrl: 'search/contactsearch.tpl.html'
                }
            },
            data:{ pageTitle: 'Contact search' }
        });
    })

    .controller( 'ContactSearchController', function ContactSearchController(
        $scope , $q, $location, $stateParams,
        $state, contactService, $log, Masterdata,
        searchService) {
        var searchInp = $stateParams.searchquery;
        //var searchInp = "sand";
        //$scope.searchquery = searchInp;
        $scope.query= searchInp;

        //searchcontacts
        /*
         householdService.getAll().then(function(allHouseholds){
         $scope.households = allHouseholds;
         });
         */
        searchService.searchContacts(searchInp).then(function(response){
            return $scope.searchcontacts = response;
        });


        $scope.getName= function getName(salutation, firstName, middleName, LastName){
            var retVal = "";
            if(salutation != null){
                retVal = salutation;
            }
            if(firstName != null){
                retVal += ( retVal.length>0 ?" " : "");
                retVal += firstName;
            }
            if(middleName != null){
                retVal += ( retVal.length>0 ?" " : "");
                retVal += middleName;
            }
            if(LastName != null){
                retVal += ( retVal.length>0 ?" " : "");
                retVal += LastName;
            }
            return retVal;
        };

        $scope.addressFormatter =  function _addressFormatter(contact){
            var strn="";
            if(contact!==null){
                if(contact.Street1 !== null) {
                    strn = contact.Street1;
                }
                if(contact.Street2 !==null) {
                    strn = strn + ( strn.length>0 ?"," : "");
                    strn += contact.Street2;
                }
                if(contact.City !==null){
                    strn = strn + ( strn.length>0 ?"," : "");
                    strn += contact.City;
                }
                if(contact.Zip !==null){
                    strn = strn + ( strn.length>0 ?"," : "");
                    strn += contact.Zip;
                }
                if(contact.CountryName !==null){
                    strn = strn + ( strn.length>0 ?"," : "");
                    strn += contact.CountryName;
                }
                if(contact.CountryShortName !==null){
                    strn = strn + ( strn.length>0 ?"," : "");
                    strn += "(" + contact.CountryShortName + ")";
                }
                if(contact.StateName !==null){
                    strn = strn + ( strn.length>0 ?"," : "");
                    strn += contact.StateName;
                }
                if(contact.StateShortName !==null){
                    strn = strn + ( strn.length>0 ?"," : "");
                    strn += "(" + contact.StateShortName + ")";
                }
            }
            return strn;
        };

        $scope.showHouseholdContact = function(householdID,contactID){
            $state.go("contactdetail",{
                "contactID": contactID,
                "householdID" : householdID
            });
        };


        $scope.rowcount =0;
        $scope.pagesize = 10;
        $scope.pagenumber = 1;
        $scope.pageBegin = 0;
        $scope.pageLimit = $scope.pagesize;
        $scope.maxNumberOfPages = 1;


        var setPagingInformation = function(){
            $scope.maxNumberOfPages = Math.ceil($scope.rowcount/$scope.pagesize);
            if( $scope.pagenumber>$scope.maxNumberOfPages) {
                $scope.pagenumber = $scope.maxNumberOfPages;
            }
            if($scope.pagenumber === 0) {
                $scope.pagenumber=1;
            }
            $scope.pageBegin = ($scope.pagenumber -1) * $scope.pagesize;
            $scope.pageLimit = $scope.pagesize;
        };
        setPagingInformation();
        $scope.changePageSize= function(){
            $log.log("pagenumber changed");
            setPagingInformation();
        };
        $scope.changePageNumber= function(){
            $log.log("pagenumber changed");
            setPagingInformation();
        };
        $scope.previousPage = function () {
            if($scope.pagenumber>1) {
                $scope.pagenumber = $scope.pagenumber - 1;
                setPagingInformation();
            }
        };
        $scope.nextPage = function () {
            if ($scope.pagenumber < $scope.maxNumberOfPages) {
                $scope.pagenumber = $scope.pagenumber + 1;
                setPagingInformation();
            }
        };
        $scope.$watchCollection ('filteredContactSearch', function(newval, oldval) {
            $log.log('Household data changed or Search key was entered');
            //$log.log(newval);
            //$log.log(oldval);
            $log.log("before :: $watchCollection=> rowcount " + $scope.rowcount);
            $scope.rowcount = newval!=null? newval.length:0;
            $log.log("after :: $watchCollection=> rowcount " + $scope.rowcount);
            setPagingInformation();
        });

    })
;