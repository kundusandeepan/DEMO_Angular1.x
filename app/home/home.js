
angular.module( 'crmAideApp.home', [
  'ui.router'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'home', {
    url: '/home',
    views: {
      "main": {
        controller: 'HomeCtrl',
        templateUrl: 'home/home.tpl.html'
      }
    },
    data:{ pageTitle: 'Home' }
  });
})

.controller( 'HomeCtrl', function HomeController( $scope, recentcontactservice ) {
  recentcontactservice.RecentContacts().then(function (data) {
    if(data!=null)
      $scope.recentcontacts = data;
    else
      $scope.recentcontacts=[];
  });
  
})

;

