/**
 * Created by Sandeepan on 4/9/2016.
 */
angular.module( 'crmAideApp.contact', [
        'ui.router',
        'ui.bootstrap'
    ])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'contactdetail', {
            url: '/:householdID/contact/:contactID',
            views: {
                "main": {
                    controller: 'ContactCtrl',
                    templateUrl: 'contact/contact.tpl.html'
                }
            },
            data:{ pageTitle: 'ContactCtrl' },
            resolve: {
                contact: [
                    '$stateParams', 'contactService',
                    function($stateParams, contactService){
                        var contactID = $stateParams.contactID;
                        return contactService.getByContactId(contactID);
                    }
                ],
                allsalutationdata:['Masterdata',  function( Masterdata){
                        return Masterdata.getSalutation();
                    }
                ],
                allcountriesdata:['Masterdata',  function( Masterdata){
                    return Masterdata.getCountries();
                }],
                allstatesdata:['Masterdata',  function( Masterdata){
                    return Masterdata.getStates();
                }],
                allserviceoptiondata:['Masterdata',  function( Masterdata){
                    return Masterdata.getServiceOption();
                }],
                allcontacttypes:['Masterdata',  function( Masterdata){
                    return Masterdata.getContactTypes();
                }]
            }
        });

        $stateProvider.state( 'addassociation', {
            url: '/:householdID/contact/:contactID/addassociation',
            views: {
                "main": {
                    controller: 'AddAssociationController',
                    templateUrl: 'contact/addassociation.tpl.html'
                }
            },
            data:{ pageTitle: 'Add Association' },
            resolve: {
                // contact: [
                //     '$stateParams', 'contactService',
                //     function($stateParams, contactService){
                //         var contactID = $stateParams.contactID;
                //         return contactService.getByContactId(contactID);
                //     }
                // ],
                // allsalutationdata:['Masterdata',  function( Masterdata){
                //     return Masterdata.getSalutation();
                // }
                // ],
                // allcountriesdata:['Masterdata',  function( Masterdata){
                //     return Masterdata.getCountries();
                // }],
                // allstatesdata:['Masterdata',  function( Masterdata){
                //     return Masterdata.getStates();
                // }],
                // allserviceoptiondata:['Masterdata',  function( Masterdata){
                //     return Masterdata.getServiceOption();
                // }],
                allcontacttypes:['Masterdata',  function( Masterdata){
                    return Masterdata.getContactTypes();
                }]
            }
        });

        $stateProvider.state( 'addcontact', {
            url: '/:householdID/addcontact/',
            views: {
                "main": {
                    controller: 'AddContactCtrl',
                    templateUrl: 'contact/addcontact.tpl.html'
                }
            },
            data:{ pageTitle: 'AddContactCtrl' },
            resolve: {
                allsalutationdata:['Masterdata',  function( Masterdata){
                    return Masterdata.getSalutation();
                }
                ],
                allcountriesdata:['Masterdata',  function( Masterdata){
                    return Masterdata.getCountries();
                }],
                allstatesdata:['Masterdata',  function( Masterdata){
                    return Masterdata.getStates();
                }]
            }
        });
    })
    .controller( 'ContactCtrl', function ContactCtrl( $scope , $q, $location, $stateParams,
                                                      $state, contactService, $log, Masterdata,
                                                      householdService, contact,
                                                      allsalutationdata, allcountriesdata,
                                                      allstatesdata, allserviceoptiondata,
                                                      contactservicedataservice,
                                                      associationservice,
                                                      ngDialog, $filter) {
        var recordid =  $stateParams.contactID;
        $scope.info = "record id : " + recordid;
        $scope.contactid = recordid;

        function parseJsonDate(jsonDateString){
            return new Date(jsonDateString);
            //return new Date(parseInt(jsonDateString.replace('/Date(', '')));
        }

        $scope.contact = contact;

        if($scope.contact!=null && $scope.contact.Association!=null){

            angular.forEach($scope.contact.Association, function(value, key) {
                //this.push(key + ': ' + value);
                Masterdata.getContactTypeNameById(value.ContactTypeID).then(function (data) {
                    value.RelationType =data;
                });
            });
            $log.log($scope.contact.Association);
        }

        if($scope.contact.DateOfBirth !== undefined && $scope.contact.DateOfBirth!=null)
        {
            $scope.DOB = parseJsonDate($scope.contact.DateOfBirth);
            var dateitem =moment($scope.contact.DateOfBirth).toDate();
            $scope.myDOB = $filter('date')(dateitem, "yyyy-MMM-dd");
        }

        $scope.$watch('myDOB' , function (value) {
            try {
                $scope.contact.DateOfBirth =moment(value).toISOString();
            } catch(e) {

            }
        });

        $scope.householdid = $scope.contact.AddressID;

        $scope.serviceOption = allserviceoptiondata;

        $scope.Salutations = allsalutationdata;


        $scope.genderList = [
            {"genderId" : true, "genderName" : "Male"},
            {"genderId" : false, "genderName" : "Female"}
        ];



        $scope.updateGender = function () {
            $log.log("gender update : " + $scope.contact.IsMale);
        };

        $scope.Update = function () {
            contactService.updateContact($scope.contact).then(function(){
                alert(" record updated");
                $state.reload();
            }, function () {
                alert(" record update failed");
            });
        };
        $scope.ChangeHousehold= function(){
            alert("change household");
        };
        $scope.AddService = function () {
            ngDialog.open({
                template: 'contact/addcontactservice.tpl.html',
                className: 'ngdialog-theme-default',
                //className: 'ngdialog-theme-search-contact',
                controller: function (ngDialog) {
                    //this.city = "New New York";
                    this.servicelist = allserviceoptiondata;
                    $log.log("this.servicelist");
                    $log.log(this.servicelist);
                    this.close= function () {
                        this.closeDialog();
                        $log.log("close");
                    };
                    this.add= function () {

                        if(angular.isDefined(this.ServiceID)){
                            //$log.log("add : " + this.ServiceID);
                            var serviceid = this.ServiceID;
                            var addservicePromise = contactservicedataservice.addServiceData(
                                $stateParams.contactID,
                                this.ServiceID,
                                this.Description);
                            addservicePromise.then(function(contactserviceid){
                                //console.log("result: " + result);
                                ngDialog.close();
                                //addressId, ContactID, contactServiceId, ServiceID
                                $scope.showdetails($scope.householdid,$stateParams.contactID,contactserviceid, serviceid);
                            });
                        }
                        else{
                            alert("Select a service to add : " );
                        }

                    };
                },
                controllerAs : "addservice"
            });
        };
        
        $scope.getServiceName= Masterdata.getServiceOptionNameById;
		
		$scope.showdetails = function(addressId, ContactID, contactServiceId, ServiceID){
			$state.go("contactservicedetail" ,{
                householdId : addressId,
                contactserviceID: contactServiceId,
                contactID : ContactID,
                serviceID : ServiceID});
		};

        $scope.showCalender = function($event) {
            $log.log("show calender");
            $event.preventDefault();
            $event.stopPropagation();

            $scope.calenderOpened = true;
        };

        $scope.updateDate = function(){
            $log.log("BEFORE::update date contact.DateOfBirth " + $scope.contact.DateOfBirth);
            $scope.contact.DateOfBirth =moment($scope.DOB).toISOString();
            $log.log($scope.contact.DateOfBirth);
            $log.log("AFTER::update date contact.DateOfBirth " + $scope.contact.DateOfBirth);
        };
        // var scope = $rootScope;
        // scope.$watch($scope.myDOB, function (newValue, oldValue) {
        //     $log.log($scope.myDOB);
        //     $scope.contact.DateOfBirth =moment($scope.myDOB).toISOString();
        //     $log.log($scope.contact.DateOfBirth);
        // });

        $scope.activities = [];
        //$scope.Association = [];

        $scope.remove = function (subscription) {
            var result = confirm("Do you want to delete?");
            if (result) {
                //Logic to delete the item
                contactservicedataservice.deleteSubscription(subscription.ContactServiceID).then(function(){
                    alert("Record deleted");
                    //$scope.refresh();
                    $state.reload();
                }, function () {
                    alert("Could not delete");
                });
            }
        };

        $scope.showContactDetail = function(householdID, contactid){
            $state.go("contactdetail",{
                "contactID": contactid,
                "householdID" : householdID
            });
        };
        
        $scope.AddAssociation = function (contact) {
            // $state.go("addassociation",{
            //     "contactID": contact.ContactID,
            //     "householdID" : contact.AddressID
            // });


            // alert("add association");
            // var dialog = ngDialog.open({
            //     template: 'contact/search.tpl.html',
            //     className: 'ngdialog-theme-default custom-width-1180',
            //     // controller: function (ngDialog) {
            //     //     this.close= function () {
            //     //         this.closeDialog();
            //     //         $log.log("close");
            //     //     };
            //     // },
            //     // controllerAs : "SearchContact"
            //     controller: 'SearchContact',
            //     resolve: {
            //         allcontact: function GetAllContacts() {
            //             return contactService.getAllContact();
            //         }
            //     }
            // });
            //
            // dialog.closePromise.then(function (data) {
            //     if(data.value!=null && data.value.ContactID >0){
            //         console.log(data.value.ContactID + ' has been selected.');
            //
            //     }
            //     // else{
            //     //     alert("Please select a contact");
            //     // }
            // });

            var dialog2 = ngDialog.open({
                template: 'contact/addassociation.tpl.html',
                className: 'ngdialog-theme-default',//custom-width-1180',
                // controller: function (ngDialog) {
                //     this.close= function () {
                //         this.closeDialog();
                //         $log.log("close");
                //     };
                // },
                // controllerAs : "SearchContact"
                controller: 'AddContactAssociation',
                resolve: {
                    allcontacttypes:  function( Masterdata){
                        return Masterdata.getContactTypes();
                    }
                }
            });
        };

        $scope.removeAssociation = function(association){
            var result = confirm("Do you want to delete?");
            if (result) {
                //Logic to delete the item
                associationservice.DeleteAssociation(association).then(function(){
                    alert("Record deleted");
                    $state.reload();
                }, function () {
                    alert("Could not delete");
                });
            }
        };

    })
    .controller( 'AddContactCtrl',
        function AddContactCtrl( $scope ,$log, contactService,
                                 $state, $stateParams,
                                 allsalutationdata, allcountriesdata,
                                 allstatesdata){

            function parseJsonDate(jsonDateString){
                return new Date(jsonDateString);
            }

            $scope.contact = {};
            $scope.contact.AddressID = $stateParams.householdID;
            $scope.DOB = null;
            $scope.householdid = $scope.contact.AddressID;
            $scope.Salutations = allsalutationdata;


            $scope.genderList = [
                {"genderId" : true, "genderName" : "Male"},
                {"genderId" : false, "genderName" : "Female"}
            ];


            $scope.updateGender = function () {
                $log.log("gender update : " + $scope.contact.IsMale);
            };

            $scope.Update = function () {
                contactService.addContact($scope.contact).then(function(data){
                    alert(" record updated");
                    $state.go("contactdetail",{
                        "contactID": data.ContactID,
                        "householdID" : data.AddressID
                    });
                }, function () {
                    alert(" record update failed");
                });
            };

            $scope.showCalender = function($event) {
                $log.log("show calender");
                $event.preventDefault();
                $event.stopPropagation();

                $scope.calenderOpened = true;
            };

            $scope.updateDate = function(){
                // $log.log("BEFORE::update date contact.DateOfBirth " + $scope.contact.DateOfBirth);
                $scope.contact.DateOfBirth =moment($scope.DOB).toISOString();
                // $log.log($scope.contact.DateOfBirth);
                // $log.log("AFTER::update date contact.DateOfBirth " + $scope.contact.DateOfBirth);
            };

            $scope.$watch('myDOB' , function (value) {
                try {
                    $scope.contact.DateOfBirth =moment(value).toISOString();
                    $log.log($scope.contact.DateOfBirth);
                } catch(e) {

                }
            });

            $scope.activities = [];
        })
    .controller( 'AddContactServiceController',
        function AddContactServiceController( $scope , close){//$stateParams, $log) {
            // $log.log("add Contact service modal UI controller ");
    })
    .controller( 'SearchContact', function SearchContact( $scope, ngDialog,allcontact,
                                                        $log, $state, $stateParams) {
        this.close= function () {
                    this.closeDialog();
                    $log.log("close");
                };
        $scope.allcontacts = allcontact;

        $scope.rowcount =0;
        $scope.pagesize = 10;
        $scope.pagenumber = 1;
        $scope.pageBegin = 0;
        $scope.pageLimit = $scope.pagesize;
        $scope.maxNumberOfPages = 1;

        var setPagingInformation = function(){
            $scope.maxNumberOfPages = Math.ceil($scope.rowcount/$scope.pagesize);
            if( $scope.pagenumber>$scope.maxNumberOfPages) {
                $scope.pagenumber = $scope.maxNumberOfPages;
            }
            if($scope.pagenumber === 0) {
                $scope.pagenumber=1;
            }
            $scope.pageBegin = ($scope.pagenumber -1) * $scope.pagesize;
            $scope.pageLimit = $scope.pagesize;
        };
        setPagingInformation();
        $scope.changePageSize= function(){
            setPagingInformation();
        };
        $scope.changePageNumber= function(){
            setPagingInformation();
        };
        $scope.previousPage = function () {
            if($scope.pagenumber>1) {
                $scope.pagenumber = $scope.pagenumber - 1;
                setPagingInformation();
            }
        };
        $scope.nextPage = function () {
            if ($scope.pagenumber < $scope.maxNumberOfPages) {
                $scope.pagenumber = $scope.pagenumber + 1;
                setPagingInformation();
            }
        };
        $scope.$watchCollection ('filteredcontacts', function(newval, oldval) {
            $scope.rowcount = newval!=null? newval.length:0;
            setPagingInformation();
        });


        $scope.selectContact = function (contact) {
            //alert("selected contact : " + contact.ContactID);
            this.closeThisDialog(contact);
        };


        $scope.AddContact = function () {
            $state.go("addcontact",
                {
                    //"contactID": -1,
                    "householdID" :$stateParams.householdID
                }
            );
            this.closeThisDialog();
        };
    })
    .controller( 'AddContactAssociation',function AddContactAssociation(
            $scope,
            allcontacttypes,
            ngDialog,
            contactService,
            associationservice,
            $state,
            $stateParams,
            $log
            ){

        $scope.allcontacttypes = allcontacttypes;

        var _getFormattedName = function (contact) {
            var contactname = "";
            if(contact.FirstName!=null && contact.FirstName.length>0){
                contactname = contact.FirstName;
            }
            if(contact.MiddleName!=null && contact.MiddleName.length>0){
                if(contactname.length>0)
                    contactname += " ";
                contactname += contact.MiddleName;
            }
            if(contact.LastName!=null && contact.LastName.length>0){
                if(contactname.length>0)
                    contactname += " ";
                contactname += contact.LastName;
            }
            return contactname;
        };
        $scope.association ={
            PrimaryContactID : $stateParams.contactID,
            RelationContactID :  null,
            ContactTypeID : null,
            IsAssociateDependent :false
        };
        $scope.SelectContact = function () {
            var dialog = ngDialog.open({
                template: 'contact/search.tpl.html',
                className: 'ngdialog-theme-default custom-width-1180',
                // controller: function (ngDialog) {
                //     this.close= function () {
                //         this.closeDialog();
                //         $log.log("close");
                //     };
                // },
                // controllerAs : "SearchContact"
                controller: 'SearchContact',
                resolve: {
                    allcontact: function GetAllContacts() {
                        return contactService.getAllContact();
                    }
                }
            });

            dialog.closePromise.then(function (data) {
                if(data.value!=null && data.value.ContactID >0){
                    console.log(data.value.ContactID + ' has been selected.');
                    $scope.association.RelationContactID = data.value.ContactID;
                    $scope.ContactName = _getFormattedName(data.value);
                }
            });
        };

        $scope.Add= function () {
                if( $scope.association.RelationContactID == null || $scope.association.RelationContactID<=0){
                    alert("Please choose Contact");
                    return;
                }
                if( $scope.association.ContactTypeID == null || $scope.association.ContactTypeID<=0){
                    alert("Please choose ContactType");
                    return;
                }
                associationservice.AddAssociation($scope.association).then(function(data){
                   alert("association added");
                    ngDialog.close();
                    $state.reload();
                });

                // $log.log("selected data" );
                // // $log.log(data);
                // $log.log($scope.association);
        };

    })
    .controller( 'AddAssociationController',function AddAssociationController(
                $scope , allcontacttypes, ngDialog) {//$stateParams, $log) {
        $scope.allcontacttypes = allcontacttypes;


        $scope.add= function () {
            alert("add clicked");
        };



        // var dialog = ngDialog.open({
        //     template: 'contact/search.tpl.html',
        //     className: 'ngdialog-theme-default custom-width-1180',
        //     // controller: function (ngDialog) {
        //     //     this.close= function () {
        //     //         this.closeDialog();
        //     //         $log.log("close");
        //     //     };
        //     // },
        //     // controllerAs : "SearchContact"
        //     controller: 'SearchContact',
        //     resolve: {
        //         allcontact: function GetAllContacts() {
        //             return contactService.getAllContact();
        //         }
        //     }
        // });

        // dialog.closePromise.then(function (data) {
        //     if(data.value!=null && data.value.ContactID >0){
        //         console.log(data.value.ContactID + ' has been selected.');
        //
        //     }
        //     // else{
        //     //     alert("Please select a contact");
        //     // }
        // });
    })

    ;
