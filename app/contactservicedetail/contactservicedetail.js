angular.module( 'crmAideApp.contactservicedetail', [
        'ui.router',
        'ui.bootstrap'
    ])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'contactservicedetail', {
            url: '/contactservicedetail/:householdId/:contactID/:contactserviceID/:serviceID',
            views: {
                "main": {
                    controller: 'ContactserviceCtrl',
                    templateUrl: 'contactservicedetail/contactservicedetail.tpl.html'
                }
            },
            data:{ pageTitle: 'Contact Service Detail' },
            resolve:{
                servicemetadata:[
                    '$stateParams','contactservicedataservice',
                    function ($stateParams,contactservicedataservice) {
                        //return contactservicedataservice.getContactServiceFieldsData($stateParams.contactID,$stateParams.serviceID);
                        return contactservicedataservice.getContactServiceFieldsData($stateParams.contactserviceID);
                    }
                ],
                serviceoptionsfielddata: [
                    'Masterdata','$stateParams', function(Masterdata,$stateParams){
                        return Masterdata.getServiceOptionFieldsById($stateParams.serviceID);//contactserviceID);
                     }
                ]
            }
        });
    })

    .controller( 'ContactserviceCtrl',
        function ContactserviceCtrl( $scope, $stateParams, Masterdata, $log,
                                     contactservicedataservice,
                                     servicemetadata,
                                     serviceoptionsfielddata) {

            //var contactId =  $stateParams.contactID;
            //var serviceId =  $stateParams.contactserviceID;

            $scope.householdid = $stateParams.householdId;
            $scope.contactid = $stateParams.contactID;
            $scope.metadatafields =serviceoptionsfielddata;
            $scope.data = servicemetadata;
            $scope.DataModel = [];

            angular.forEach($scope.data.ContactServiceField, function(value, idx) {
                this[value.ServiceFieldID] = value.ServiceFieldValue;
            },$scope.DataModel);

            var prefix = "crm_id_";
            $scope.getFieldName= function(fieldIndex){
                return prefix +   fieldIndex;
            };
            
            $scope.Update= function () {
                if($stateParams.contactserviceID > 0) {
                    var keyvaluepair = {};
                    angular.forEach($scope.DataModel, function(value, idx, allarray) {
                        this[idx +""] = value;
                    },keyvaluepair);

                    contactservicedataservice.updateSubscription($stateParams.contactserviceID,keyvaluepair ).then(function () {
                        alert("Subscription updated");
                    }, function () {
                        alert("Subscription update failed");
                    });

                }
                
            };
    });
