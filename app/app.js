angular.module( 'crmAideApp', [
  'templates-app',
  'templates-common',
  'crmAideApp.home',
  'crmAideApp.about',
  'crmAideApp.auth',
  'crmAideApp.household',
  'crmAideApp.householddetail',
  'crmAideApp.contact',
  'crmAideApp.contactservicedetail',
  'crmAideApp.contactsearch',
  'crmAideApp.report',
  'crmAideApp.assistant',
  'crmAideApp.help',
  'crmAideApp.setting',
  'crmAideApp.profile',
  'crmAideApp.forgotpassword',
  'ui.mask',
  'ui.router',
  'ui.calendar',
  'ui.bootstrap',
  'angucomplete-alt',
  //'angularModalService',
  'ngDialog',
  //'ngFlash'
  //'ui.utils'
  'ngAria',
  'ngAnimate',
  'ngMaterial',
  'ngMessages',
  '720kb.datepicker',
  '720kb.tooltips',
  'textAngular',
  'ui.bootstrap.datetimepicker',
  'treeControl',
  'FBAngular',
  'angular-loading-bar',
  'ngSanitize',
  'ngCsv'//,
    // 'jsPDF'
  //'ng.httpLoader'
  //'darthwade.dwLoading'
  //'ui.tree'
])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider, cfpLoadingBarProvider){
  cfpLoadingBarProvider.spinnerTemplate = '<div><span class="fa fa-spinner">Loading...</div>';
  // var tmpl = '<div class="container container-table">'+
  //           '<div class="row vertical-center-row">'+
  //           '<div class="text-center col-md-4 col-md-offset-4" style="background:red">'+
  //           '<button class="btn btn-lg btn-info"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading...</button>'+
  //           '</div>'+
  //           '</div>'+
  //           '</div>';
  // cfpLoadingBarProvider.spinnerTemplate = tmpl;
      //cfpLoadingBarProvider.includeBar = false;
  cfpLoadingBarProvider.includeSpinner = false;
  $urlRouterProvider.otherwise( '/home' );
})

.run(function ($rootScope,$state) {
  $rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
    if(error === "notauthenticated"){
      $state.go("auth");
    }
  });
})
.controller( 'AppCtrl', function AppCtrl ( $scope , $log, householdService, authService,$state, searchService, httpRequestTracker) {
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
    var annonymousAllowedStates = ["about","auth"];
    var isWhiteListedState = (annonymousAllowedStates.indexOf(toState.name)!==-1);
    if(!isWhiteListedState && !authService.isLoggedIn()){
      $state.go("auth");
      event.preventDefault();
    }
  });
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | crmAideApp' ;
    }
    $scope.isLoggedIn =  authService.isLoggedIn();
    var userInfo = authService.getUserInfo();
    $scope.AvatarName = (userInfo !== undefined && userInfo !== null)? userInfo.AvatarName: "";
  });

  $scope.searchquery = "";
  $scope.doContactSearch = function () {
    var searchparam = $scope.searchquery;
    $scope.searchquery='';
    $state.go("contactsearch",{searchquery:searchparam});
  };

  $scope.signout = function(){
    var _callback= function(){
      authService.clearCredentials();
      $state.go("auth");
    };
    authService.logout().then(_callback, _callback);
  };

  $scope.hasPendingRequests = function () {
    return httpRequestTracker.hasPendingRequests();
  };

});

