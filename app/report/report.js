/**
 * Created by Sandeepan on 5/10/2016.
 */
angular.module( 'crmAideApp.report', [
        'ui.router',
        'ui.bootstrap'
    ])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'report', {
            url: '/report',
            views: {
                "main": {
                    controller: 'ReportController',
                    templateUrl: 'report/report.tpl.html'
                }
            },
            data:{ pageTitle: 'report' },
            resolve:{
                treedata:['reportservice',function (reportservice) {
                    return reportservice.GetTree();
                }]
            }
        });
        $stateProvider.state( 'report.type', {
            url: '/:reporttype/:reportid/:reportcaption',
            views: {
                "report-input": {
                    controller: 'ReportByIdController',
                    templateUrl: 'report/reportInput.tpl.html'
                }
            },
            data:{ pageTitle: 'report' },
            resolve:{
                metadata:['reportservice','$stateParams',function (reportservice, $stateParams) {
                    return reportservice.GetMetadata($stateParams.reportid);
                }]
            }
        });
    })
    .controller( 'ReportController', function ReportController( $scope,treedata,$log,$state ) {
        $scope.expandedNodes =[];

        var populateExpandedNodes = function(nodes,childPropertyName ){
            angular.forEach(nodes, function(value, key){
                populateExpandedNode(value, childPropertyName);
            });
        };
        var populateExpandedNode = function(node,childPropertyName ){
            var childrens = node[childPropertyName];
            if(childrens.length>0)
            {
                $scope.expandedNodes.push(node);
                populateExpandedNodes(childrens,childPropertyName);
            }
        };

        populateExpandedNodes(treedata,"Children");
       
        $scope.treeOptions = {
            nodeChildren: "Children",
            dirSelectable: true,
            injectClasses: {
                ul: "a1",
                li: "a2",
                liSelected: "a7",
                iExpanded: "a3",
                iCollapsed: "a4",
                iLeaf: "a5",
                label: "a6",
                labelSelected: "a8"
            }
        };

        $scope.dataForTheTree =treedata;
        $scope.showSelected = function(node) {
            if(node.NodeType != "category") {
                $state.go("report.type",{
                    "reportid":node.Id,
                    "reporttype":node.NodeType,
                    "reportcaption" : node.Name
                });
            }
            if(node.NodeType == "category") {
                var idx = $scope.expandedNodes.indexOf(node);
                if( idx !== -1) {
                    $scope.expandedNodes.splice(idx, 1);
                }
                else{
                    $scope.expandedNodes.push(node);
                }
            }
        };

    })
    .controller( 'ReportByIdController', function ReportByIdController(
                                $scope,$log,$state,$stateParams,
                                reportservice,metadata ) {
        
        $scope.metadatafields = metadata;
        $scope.DataModel = [];
        $scope.reportcaption =  $stateParams.reportcaption;
        $scope.firsttime = true;

        angular.forEach($scope.metadata, function(value, idx) {
            this[value.Id] = '';
        },$scope.DataModel);

        $scope.getReport= function(){
            $scope.firsttime = false;
            var errormessage="";
            var nameValuePairs = {};
            angular.forEach(metadata, function(value, idx) {
                //this[value.Id] = '';
                if(angular.isDefined( $scope.DataModel[value.Id])) {
                    this[value.Id]=$scope.DataModel[value.Id];
                }
                else{
                    errormessage = "Value for " + value.FieldCaption + " not selected.";
                }
            },nameValuePairs);
            if(errormessage !== ""){
                alert("kindly select  all the value. " + errormessage + "cannot continue.");
                return;
            }

            var postdata = {
                FieldValuePair : nameValuePairs,
                ReportId : $stateParams.reportid
            };
            // $log.log(postdata);
            // alert("now fetch report data");
            // move this to
            $scope.showdata =false;
            $scope.getAlignCss = function (data) {
                //console.log("isnumber: (" + data + ") : " +angular.isNumber(data));
                return angular.isNumber(data) ? "text-right":"";
            };
            // $scope.downloadCSV = function () {
            //     alert("download csv");
            // };
            $scope.pdfinfo = {rows:[],columns:[]};
            $scope.downloadPDF = function () {
                /*jshint newcap:false*/
                var el = document.getElementById("report-table");
                if (el != null) {
                    var doc = new jsPDF('p', 'pt');
                    doc.autoTable($scope.pdfinfo.columns, $scope.pdfinfo.rows);
                    doc.save($scope.reportcaption + '.pdf');
                }
            };
            $scope.setEmptyData= function () {
                $scope.showdata =false;
                $scope.data=[];
                $scope.dataheader = [];
                $scope.pdfinfo.rows = [];
                $scope.pdfinfo.columns = [];
            };
            switch(angular.lowercase($stateParams.reporttype)){
                case "report":
                    reportservice.GetReport(postdata).then(function (data) {
                        if(data === null || data === undefined || data.length===0){
                            $scope.setEmptyData();
                            return;
                        }
                        $scope.data = data;
                        $scope.showdata = true;

                        // (key, value) in data[0]
                        $scope.dataheader = [];
                        $scope.pdfinfo.rows = $scope.data;
                        $scope.pdfinfo.columns = [];
                        angular.forEach($scope.data[0], function (value, key) {
                            $scope.dataheader.push(key);
                            $scope.pdfinfo.columns.push({title: key, dataKey: key});
                        });

                    },function () {
                        setEmptyData();
                    });

                    break;
                case "graph":
                    alert("graph data not avilable now");
                    // reportservice.GetGraph(postdata).then(function (data) {
                    //     $scope.graphreport = data;
                    //     // $scope.$parent.graphreport = data;
                    // });
                    break;
                default:
                    //do nothing
                    break;
            }
            //$state.go("report.type.output",{});
        };


        //============== FULL SCREEN BEHAVIOUR START ======================
        $scope.isFullscreen = false;
        $scope.toggleFullScreen = function() {
            $scope.isFullscreen = !$scope.isFullscreen;
        };
        // $scope.toggleFullScreenReport= function () {
        //
        // };
        $scope.goFullScreenViaWatcher = function() {
            $scope.isFullScreen = !$scope.isFullScreen;
        };
        //============== FULL SCREEN BEHAVIOUR END ======================

    })
;
